@Grab(group = 'org.apache.httpcomponents', module = 'httpclient', version = '4.5.2')
import java.text.SimpleDateFormat
import groovy.json.*
import groovy.xml.MarkupBuilder
import org.apache.http.client.methods.*
import org.apache.http.impl.client.*
import java.text.DateFormat



//loading from properties file
def propertiesFileLocation = "JiraUtils.properties"
Properties properties = readProperties(propertiesFileLocation)


// credentials
user = properties.username
passEncodedBase64 = properties.password//pass encoded base64
pass = new String(passEncodedBase64.decodeBase64())
def beginTime = properties.beginTime
matchingDays = calculateDatesToSkipBasedOnJiraCall(beginTime)
if ("lastTwoWeeks()" == beginTime){
    beginTime = "startOfDay('-14d')"
}


//jira settings - url and jql

def jiraBaseURL = "https://" + properties.jiraContext + ".atlassian.net/"
jiraIssueBaseURL = jiraBaseURL + "browse/"
def jiraRestURL = jiraBaseURL + "rest/api/latest/"
def endTime = "now()"
def searchJqlBase = "search?jql="
def userToCheck = "currentUser()" // currentUser is user logged in
def query = searchJqlBase + "worklogAuthor%20%3D%20" + userToCheck + "%20and%20worklogDate%20%3C%20" + endTime + "%20and%20worklogDate%20%3E%20" + beginTime + "&fields=worklog" + "&fields=summary"
//outputFile
def fileOutputPath = "reportTable.html"
println "requesting data from $jiraRestURL"
def get = new HttpGet(jiraRestURL + query)


def resultMap = getResponse(get)
def mapDateToTimeLogged = getIssuesAndWorklogsToDateMap(resultMap, user)


println "fetching data done. building report..."



Map<String, String> issuenames = [:]
resultMap.issues.each {
    issuenames.put(it.key, it.fields.summary)
}

Set<String> setOfIssues = []
mapDateToTimeLogged.values().each {
    setOfIssues += it.keySet()
}

println "Creating HTML file."
def file1 = new File(fileOutputPath)
file1.exists() ? file1.delete() : println("Creating new file")
matchingDays.retainAll(mapDateToTimeLogged.keySet().asList())
file1 << createTable(matchingDays, setOfIssues, mapDateToTimeLogged, issuenames)
println "report built. enjoy your report. brought to you by BIOS"



def getIssuesAndWorklogsToDateMap(resultMap, user) {
    TreeMap<String, TreeMap<String, TreeMap<String, Double>>> mapDateToTimeLogged = [:]
    resultMap.issues.each { issue ->
        def worklogUrl = new HttpGet(issue.self + "/worklog" as String)
        println worklogUrl
        Object resp = getResponse(worklogUrl)
        resp.worklogs.findAll { it.author.name == user }.each { logWork ->
            def date = logWork.started.substring(0, 10)
            def timeSpent = (double) logWork.timeSpentSeconds / 60 / 60
            if (mapDateToTimeLogged.get(date) == null) {
                Map<String, Map<String, Double>> issueKeyToMapOfUpdateIssueAndTimeSpent = new TreeMap<>()
                issueKeyToMapOfUpdateIssueAndTimeSpent.put(issue.key, createMapForUpdateIssueAndTimeSpent(logWork, timeSpent))
                mapDateToTimeLogged.put(date, issueKeyToMapOfUpdateIssueAndTimeSpent)
            } else {
                if (mapDateToTimeLogged.get(date).get(issue.key) != null) {
                    def mapToAdd = mapDateToTimeLogged.get(date).get(issue.key)
                    mapToAdd.put(jiraUpdateUrl(logWork.id, logWork.issueId), timeSpent)
                    mapDateToTimeLogged.get(date).put(issue.key, mapToAdd)
                } else {
                    mapDateToTimeLogged.get(date).put(issue.key, createMapForUpdateIssueAndTimeSpent(logWork, timeSpent))
                }
            }
        }
    }
    mapDateToTimeLogged
}

private static createMapForUpdateIssueAndTimeSpent(logWork, double timeSpent) {
    Map<String, Double> worklogIdToWorkSpent = new TreeMap<>()
    worklogIdToWorkSpent.put(jiraUpdateUrl(logWork.id, logWork.issueId), timeSpent)
    worklogIdToWorkSpent
}

Object getResponse(HttpGet self) {
    self.addHeader("content-type", "application/json")
    self.addHeader('Authorization', 'Basic ' + "$user:$pass".bytes.encodeBase64().toString())
    def hcb = HttpClientBuilder.create().build()
    def respHcb = hcb.execute(self)
    def br = new BufferedReader(new InputStreamReader(respHcb.getEntity().getContent()))
    def jp = br.getText()
    def js = new JsonSlurper()
    if (jp.contains("errorMessages")) {
        println jp
    }
    def resp
    try {

        resp = js.parseText(jp)
    } catch (Exception e) {
        println jp
        println e.getStackTrace()
        throw e
    }

    resp
}

def createTable(columns, rows, map, issueNames) {
    def writer = new StringWriter()
    def mb = new MarkupBuilder(writer)

    mb.html() {
        head {
            style(type: "text/css", ''' 
                table {
                    border-collapse: collapse;
                    width: 100%;
                }
                span {
                   display: block;
                }

                th, td {
                    width 100%;
                    text-align: center;
                    border: 1px solid #ddd;
                    padding: 8px;
                }

                tr:nth-child(even){background-color: #f2f2f2}
                tr:hover {background-color: #cac;}
                td.red {
                    background-color: red;
                }
                
                a.red {
                    font-size:20px;
                    color: red;
                }
                a.green {
                    font-size:20px;
                    color: green;
                }
                td.description {
                    overflow:hidden;
                    text-overflow: ellipsis;
                    white-space: nowrap;
                    max-width: 200px;
                }
                
                th {
                    background-color: #4CAF50;
                    color: white;
                    border: 1px solid #000;
                    text-align: center !important;
                    padding: 3px !important;
                }           
            ''')
            link(rel: "stylesheet", href: "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css", '')
            script(src: "https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js", '')
            script(src: "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js", '')
        }
        body {
            div(class: "jumbotron text-center") {
                h1('Who needs tempo plugin?')
                p('You have no idea what you logged and where?')
                p('Excel sheet does not work for some magical reason?')
                p('Have no fear!')
                p('BIOS is here!')
            }
            div(class: "container-fluid") {
                table {
                    tr() {
                        th('Issue number')
                        th('Description')
                        columns.each { title -> th(title.substring(5)) }
                    }
                    rows.each { issue ->
                        tr() {
                            td() {
                                a(href: jiraIssueBaseURL + issue, issue, title: issueNames.get(issue))
                                a(href: jiraAddUrl(issue), class: "green", "+")
                            }
                            td(class: "description") {
                                a(href: jiraIssueBaseURL + issue, issueNames.get(issue), title: issueNames.get(issue))
                            }
                            columns.each { day ->
                                if (map[day][issue] != null) {
                                    td() {
                                        map[day][issue].each {
                                            a(href: it.key, it.value)
                                            a(href: jiraRemoveUrl(it.key), class: "red", "x")
                                            span()
                                        }
                                    }
                                } else {
                                    td() {
                                    }
                                }
                            }
                        }

                    }
                    tr {
                        td("Summary: ")
                        td()
                        columns.each { day ->
                            def summary = 0

                            rows.each { issue ->
                                map[day][issue].each {
                                    summary += it.getValue()
                                }
                            }
                            if (summary < 8.0) {
                                td(class: "red", summary)
                            } else {
                                td(summary)
                            }
                        }
                    }
                }

            }
        }
    }
    writer.toString()
}

def static jiraRemoveUrl(String jiraUpdateUrl) {
    return jiraUpdateUrl.replaceAll("UpdateWorklog", "DeleteWorklog")
}

def static jiraAddUrl(String jiraIssueKey) {
    return "https://iciskm.atlassian.net/secure/CreateWorklog!default.jspa?key=" + jiraIssueKey
}

def static jiraUpdateUrl(worklogId, issueId) {
    //issue Id can be handled by issue key - so easier to maintain. Good idea to improve it.
    return "https://iciskm.atlassian.net/secure/UpdateWorklog!default.jspa?id=" + issueId + "&worklogId=" + worklogId
}

private static List getMatchingDays(day, dayInTimePeriod, int periodLength) {

    Calendar c = Calendar.getInstance()
// Set the calendar to monday of the current week
    c.set(day, dayInTimePeriod)

// Print dates of the current week starting on Monday
    buildMatchingDaysList(periodLength, c)
}

private static List buildMatchingDaysList(int periodLength, Calendar c) {
    println c.getTime()

    def matchingDays = []
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd")
    for (int i = 0; i < periodLength; i++) {
        matchingDays.add(df.format(c.getTime()))
        c.add(Calendar.DATE, 1)
    }
    matchingDays
}

private static List lastTwoWeeks() {
    Calendar c = Calendar.getInstance()
    c.add(Calendar.DAY_OF_YEAR, -14)

    buildMatchingDaysList(14, c)
}

def static calculateDatesToSkipBasedOnJiraCall(jiraCall) {

    Calendar c = Calendar.getInstance()
    switch (jiraCall) {

        case "startOfWeek()":
            println "startOfWeek"
            return getMatchingDays(Calendar.DAY_OF_WEEK, c.MONDAY, 7)
            break
        case "lastTwoWeeks()":
            println "lastTwoWeeks"
            return lastTwoWeeks()
            break
        case "startOfMonth()":
            println "startOfMonth"
            return getMatchingDays(Calendar.DAY_OF_MONTH, 1, c.getInstance().getActualMaximum(c.DAY_OF_MONTH))
            break
        case "startOfYear()":
            println "startOfYear"
            return getMatchingDays(Calendar.DAY_OF_YEAR, 1,  c.getInstance().getActualMaximum(c.DAY_OF_YEAR))
            break
    }
}

private Properties readProperties(String propertiesFilePathName) {
    Properties properties = new Properties()
    File propertiesFile = new File(propertiesFilePathName)
    println "getting input data from $propertiesFile.absolutePath"
    propertiesFile.withInputStream {
        properties.load(it)
    }
    properties
}